#!bin/sh
cat <<EOF

set lmargin 5
set rmargin 1
set tmargin 0
set xrange [0:1]
set yrange [0:1]
set zrange [-0.3:0.3]
set dgrid3d 50,200
set isosamples 100
set pm3d
unset surface
set view 0,0,1
set palette rgbformulae 22,13,-31
set term png
EOF


for file in p*0.dat
do
pfile=$file
nfile=`echo $file | sed -e s/^p/n/;`
ffile=`echo $file | sed -e s/^p/f/;`
xfile=`echo $file | sed -e s/^p/x/;`
pngfile=`echo $file | sed -e s/dat$/png/;`
movfile=`echo $pngfile | sed -e s/^p/contour_/;`

cat << EOF
set size 1,1
set out"$movfile"
set multiplot

set size 1,0.3
set origin 0,0
set format x""
set bmargin 0
set tmargin 0
splot"$ffile"u 1:2:4 w l

set size 1,0.3
set origin 0,0.3
set format x""
set bmargin 0
set tmargin 0
splot"$ffile"u 1:2:8 w l

set size 1,0.3
set origin 0,0.6
set format x
set bmargin 0
set tmargin 0
splot"$ffile"u 1:2:3 w l

unset multiplot

EOF

done

echo set term x11

#set nomultiplot
