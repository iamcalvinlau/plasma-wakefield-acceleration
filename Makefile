###################################
### makefile for one-d-pic code ###
###################################

#F90 = sxf90
F90 = ifort -openmp
#FFLAGS = -P auto -p -R5 -lasl -ftrace
#FFLAGS = -p -R5 -ftrace -lasl

LD = $(F90)
#LDFLAGS = -P auto -p -lasl -ftrace
#LDFLAGS = -p -ftrace -lasl

LOAD = pwfa.out
SRCS = param.f90 main.f90 initial.f90 density.f90 fsolve.f90 motion.f90 energy.f90 history.f90 print.f90
OBJS = $(SRCS:.f90=.o)

all:$(LOAD)
$(LOAD):$(OBJS)
	$(LD) $(OBJS) $(LDFLAGS) -o $@

%.o:%.f90
	$(F90) $(FFLAGS) -c $<


clean:
	rm -f *.o *~ *.L *.out *.mod *.dat out CONDITION RUN_TIME pwfa.e* pwfa.o*

distclean: clean
	rm -f *.a $(PRG) $(MAINPRG).tar.gz $(MAINPRG).dat debug.* core gmon.out

