subroutine energy(e,b,t,u,x)
  
  use param , only : gam0, m, dx, dt, econ, n0, mass,&
                     runnum, time, pulselength, ss
  
  implicit none
  
  integer, intent(in) :: t
  real(8), intent(in) :: e(:,:), b(:,:), u(:,:,:), x(:,:,:)
  
  real(8) :: pe, ke, te, emaxpos
  real(8) :: pm(1:3), km(1:3), tm(1:3)
  real(8) :: kmax(1:2), temp(1:n0,1:2), temp_pond(1:n0,1:2), kpondmax(1:2)
  real(8), save:: pe0, ke0, e_max_pos_left, e_max_pos_right
  real(8), save:: pm0(1:3), km0(1:3)
  integer :: i, s, iemaxpos


!!! calculate energy and momentum !!!
  pe = econ*(sum((e(1:m-1,1:3)**2) + (b(1:m-1,1:3)**2)))
  pm(1) = 2.0*econ*(sum(e(1:m-1,2)*b(1:m-1,3)-e(1:m-1,3)*b(1:m-1,2)))
!  pm(2) = 2.0*econ*(sum(e(1:m-1,3)*b(1:m-1,1)-e(1:m-1,1)*b(1:m-1,3)))
!  pm(3) = 2.0*econ*(sum(e(1:m-1,1)*b(1:m-1,2)-e(1:m-1,2)*b(1:m-1,1)))

  pe = pe/real(2*n0)
!  pm(1:3) = pm(1:3)/real(2*n0)
  pm(1) = pm(1)/real(2*n0)

  ke=0.0d0
  km(1) = 0.0d0

  iemaxpos=1
  e_max_pos_left=(dx*maxloc(e(:,2),1))-(pulselength)
  e_max_pos_right=(dx*maxloc(e(:,2),1))+(pulselength)
  if (e_max_pos_left<0.0) e_max_pos_left=e_max_pos_left+ss
  if (e_max_pos_right>=ss) e_max_pos_right=e_max_pos_right-ss
  if (e_max_pos_left>e_max_pos_right) iemaxpos=0
  temp_pond(1:n0,1:2)=0.0

  do s=1,2
!!$omp parallel do private(i)
     do i=1,n0
        temp(i,s) = sqrt(1.0 + dot_product(u(i,1:3,s),u(i,1:3,s)))
        if (iemaxpos==1) then
          if (e_max_pos_left<(x(i,1,s)*dx) .and. (x(i,1,s)*dx)<e_max_pos_right) then
            temp_pond(i,s)=temp(i,s)
          else
            temp_pond(i,s)=0.0
          endif
        elseif (iemaxpos==0) then
          if (e_max_pos_left<(x(i,1,s)*dx) .and. (x(i,1,s)*dx)<=ss) then
            temp_pond(i,s)=temp(i,s)
          elseif ((x(i,1,s)*dx)<e_max_pos_right .and. 0.0<=(x(i,1,s)*dx)) then
            temp_pond(i,s)=temp(i,s)
          else
            temp_pond(i,s)=0.0
          endif
        endif
        ke = ke + mass(s)*sqrt(1.0 + dot_product(u(i,1:3,s),u(i,1:3,s)))
!        km(1:3) = km(1:3) + mass(s)*u(i,1:3,s)
        km(1) = km(1) + mass(s)*u(i,1,s)
     end do

!!$omp end parallel do
  end do
! solve for maximum kinetic energy 
  kmax(1) = mass(1)*maxval(temp(:,1))/real(2*gam0)
  kmax(2) = mass(2)*maxval(temp(:,2))/real(2*gam0)

  kpondmax(1) = mass(1)*maxval(temp_pond(:,1))/real(2*gam0)
  kpondmax(2) = mass(2)*maxval(temp_pond(:,2))/real(2*gam0)

  ke = ke/real(2*n0*gam0)
!  km(1:3) = km(1:3)/real(2*n0*gam0)
  km(1) = km(1)/real(2*n0*gam0)
  
  if((t+(runnum*time))==0) then
     pe0 = pe
     ke0 = ke
!     pm0(1:3) = pm(1:3)
!     km0(1:3) = km(1:3)
     pm0(1) = pm(1)
     km0(1) = km(1)

!      write(*,*) "pe0 =",pe0
!      write(*,*) "ke0 =",ke0
!      write(*,*) "te0 =",pe0+ke0
!      write(*,*) "pm0(1:3) =",pm0(1),pm0(2),pm0(3)
!      write(*,*) "km0(1:3) =",km0(1),km0(2),km0(3)
!      write(*,*) "tm0(1:3) =",pm0(1)+km0(1),pm0(2)+km0(2),pm0(3)+km0(3)
      
  end if

  !pe = pe - pe0
  !ke = ke - ke0
  te = pe + ke
!  pm(1:3) = pm(1:3) - pm0(1:3)
!  km(1:3) = km(1:3) - km0(1:3)
!  tm(1:3) = pm(1:3) + km(1:3)
  pm(1) = pm(1) - pm0(1)
  km(1) = km(1) - km0(1)
  tm(1) = pm(1) + km(1)

  write(18,'(8e15.7)') (t+(runnum*time))*dt, pe, ke, te, kmax(1), kmax(2), kpondmax(1), kpondmax(2)
  write(22,'(4e15.7)') (t+(runnum*time))*dt, pm(1), km(1), tm(1)
!  write(23,'(4e15.7)') t*dt, pm(2), km(2), tm(2)
!  write(24,'(4e15.7)') t*dt, pm(3), km(3), tm(3)

end subroutine energy
