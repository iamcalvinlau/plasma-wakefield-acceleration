subroutine history(x,u,e,b,cur,t,pden,nden,pondf)
  
  use param , only : m, dx, dt, n0, runnum, time, q
  
  implicit none
  
  integer, intent(in) :: t
  real(8), intent(in) :: pden(:), nden(:)
  real(8), intent(in) :: e(:,:), b(:,:), cur(:,:), pondf(:,:)
  real(8), intent(in) :: x(:,:,:), u(:,:,:)
  
  real(8) :: gamt(1:2)
  integer :: i, k
  character :: filen*7
  

  
  write(filen,'(i7.7)') t+(runnum*time)
  open(30, file='f'//filen//'.dat')
  open(33, file='p'//filen//'.dat')
  open(34, file='n'//filen//'.dat')

!!! calc ponderomotive force: q(vy) cross Bz !!!

  do i=1,m-1
!!! grid information !!!
     write(30,'(11e15.7)') real(i)*dx,e(i,1),e(i,2),e(i,3),b(i,1),b(i,2),b(i,3),cur(i,2),cur(i,3),pden(i),nden(i) 
  end do


!!! particles in phase space !!!
  do k=1,n0
     gamt(1) = sqrt(1.0 + dot_product(u(k,1:3,1),u(k,1:3,1)))
     gamt(2) = sqrt(1.0 + dot_product(u(k,1:3,2),u(k,1:3,2)))
     write(33,'(6e15.7)') x(k,1,1)*dx, u(k,1,1), u(k,2,1), u(k,3,1), gamt(1), pondf(k,1)
     write(34,'(6e15.7)') x(k,1,2)*dx, u(k,1,2), u(k,2,2), u(k,3,2), gamt(2), pondf(k,2)
  end do

  
  close(30)
  close(33)
  close(34)
  
end subroutine history
