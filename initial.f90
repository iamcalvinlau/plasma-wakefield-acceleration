subroutine initial(e,b,x,u,v,pden,nden,pondf)

  use param , only :n0, m, B0, E0, ppg, sigma, pi, wavenum
  use param , only :ss, ts, dx, time, pi, cc, econ ,q, dt, pulselength
  use param , only :ub, vb, gamb, uth, vth, gamth, dl, casenum
  use omp_lib  

  implicit none
  
  real(8), intent(out) :: pden(:), nden(:)  
  real(8), intent(out) :: e(:,:), b(:,:), pondf(:,:)
  real(8), intent(out) :: x(:,:,:), u(:,:,:), v(:,:,:)

  integer :: s, k, i, l, lmax, num, cyc, tmp
  real(8) :: xh, tempf
  real(8) :: gamt, r(1:6)
  real(8) :: a1,a2,a3
  real(8), allocatable :: r1(:), r2(:), r3(:)
  real(8) :: f, fp, df, beta, crit, theta, phi, dif
  real(8) :: gausstemp1,gausstemp2,gausstemp3  
  integer :: ioffset
  integer :: nthreads

!!!!!!!!!!!!!!!!!!! print out CONDITION  !!!!!!!!!!!!!!!!!!
  open(11, file='CONDITION')

!$omp parallel private(nthreads)
  nthreads = omp_get_num_threads()
!$omp single
  write(11,'(/,"===================================")')
  write(11,*)' Number of OpenMP threads = ',nthreads
  write(11,'("===================================",/)')
!$omp end single nowait
!$omp end parallel

  write(11,'(a15,i15)') 'N of particles =',n0,"*2"
  write(11,'(a15,i15)') 'N of grids =',m
  write(11,'(a15,i15)') 'time steps =',time
  write(11,'(a15,i15)') 'N of particles/grid =', ppg
  write(11,'(a15,e15.5)') 'econ =', econ
  write(11,'(a15,e15.5)') 'cc =', cc
  write(11,'(a15,e15.5)') 'dx = dt =', dx,"/gyro motion"
  write(11,'(a15,e15.5)') 'ts =', ts,"/gyro period"
  write(11,'(a15,e15.5)') 'ss =', ss,"gyro radius"
  write(11,'(a15,e15.5)') 'p-vth =', vth(1)
  write(11,'(a15,e15.5)') 'p-gamth =', vth(1)/sqrt(1.0 - vth(1)**2)
  write(11,'(a15,e15.5)') 'e-vth =', vth(2)
  write(11,'(a15,e15.5)') 'e-gamth =', vth(2)/sqrt(1.0 - vth(2)**2)
  write(11,'(a15,e15.5)') 'sigma =', sigma
  write(11,'(a15,e15.5)') 'Debye length/dx =', dl
  write(11,'(a15,e15.5)') 'ey0 = bz0 =', e0(2)
  write(11,'(a15,i15)') 'N of waves/pulse =', wavenum

  close(11)
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  

  do s=1,2
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!               non-relativistic Maxwellian              !!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
     if(vth(s)<=0.1) then
        
        if(s==1) then 
           write(*,*) "positive charge is cold"
        else  
           write(*,*) "negative charge is cold"
        end if
        
        do k=1,n0
           call random_number(r(1:6))      
           a1 = vth(s)*sqrt(-2.0*log(r(1)))*cos(2.0*pi*r(2))
           a2 = vth(s)*sqrt(-2.0*log(r(3)))*cos(2.0*pi*r(4))
           a3 = vth(s)*sqrt(-2.0*log(r(5)))*cos(2.0*pi*r(6))

           v(k,1,s) = (vb(s) + a1)/(1.0 + vb(s)*a1)
           v(k,2,s) = a2/(gamb(s)*(1.0 + vb(s)*a1))
           v(k,3,s) = a3/(gamb(s)*(1.0 + vb(s)*a1))

           if(v(k,1,s)>=1.0 .or. v(k,2,s)>=1.0 .or. v(k,3,s)>=1.0) then
              write(*,*) "false generating particle velocity"
              write(*,*) v(k,:,s)
              stop
           end if
           
           
           gamt = 1.0/sqrt(1.0 - dot_product(v(k,1:3,s),v(k,1:3,s)))
           u(k,1:3,s) = v(k,1:3,s)*gamt
           xh = 1.0 + real(k)/real(n0)*real(m-1)
           x(k,1,s) = xh + 0.5*v(k,1,s)
           
           if(x(k,1,s)>=real(m)) then
              x(k,1,s) = x(k,1,s) - real(m) + 1.0
           else if(x(k,1,s)<1.0) then
              x(k,1,s) = x(k,1,s) + real(m) - 1.0
           end if
        end do



        
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!                relativistic Maxwellian                  !!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
     else
        
        if(s==1) then 
           write(*,*) "positive charge is hot"
        else  
           write(*,*) "negative charge is hot"
        end if
         
        beta = 1.0/(gamth(s) - 1.0)
        crit = 1.0d-6 !! precision
        lmax = 1000 !! maximum iteration number
        num = 2*n0
        cyc = 0
        k = 0
        i = 1
        
        allocate(r1(1:num))
        allocate(r2(1:n0))
        allocate(r3(1:n0))     
        call  random_number(r1(1:num))
        call  random_number(r2(1:n0))
        call  random_number(r3(1:n0))
        
        do while(i<=n0)
           
           k = k + 2
           if(k>num) then
              k = 1
              call random_number(r1(1:num))
           end if
           
           fp = 0.0d0
           df = 1.0d0
           l = 0

           do while(abs(df)>crit .and. l<lmax)
!!!!!!!!!!!!!!!!!!!!!!!!!!!! 3-dimension !!!!!!!!!!!!!!!!!!!!!
              f = log(0.5*((fp+1.0)**2 + 1)/(1.0 - r1(k)))
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
              
!!!!!!!!!!!!!!!!!!!!!!!!!!!! 2-dimension !!!!!!!!!!!!!!!!!!!!
!              f = log((fp+1.0)/(1.0 - r1(k)))
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
              
              df = f - fp
              fp = f
              l = l + 1
           end do
           
           f = f/beta
           dif = exp(-beta*(sqrt(1.0 + f**2) - abs(f)))
           
           if(r1(k+1)>dif) then
              cyc = cyc + 1              
              cycle
           end if
           
!!!!!!!!!!!!!!!!!!!! 3-dimension !!!!!!!!!!!!!!!!!!!!!!!!
           theta = 2.0*(r2(i)-0.5)
           phi = r3(i)*2.0*pi
           
           u(i,1,s) = f*theta
           u(i,2,s) = f*sqrt(1.0 - theta**2)*sin(phi)
           u(i,3,s) = f*sqrt(1.0 - theta**2)*cos(phi)
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
           
           
!!!!!!!!!!!!!!!!!!!! 2-dimension !!!!!!!!!!!!!!!!!!!!!!!!
!            phi = (r3(i) - 0.5)*2.0*pi
!            u(i,1,s) = f*cos(phi)
!            u(i,2,s) = f*sin(phi)
!            u(i,3,s) = 0.0d0
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
           
           
           u(i,1,s) = sqrt(1.0 + f**2)*ub(s) + gamb(s)*u(i,1,s)
           i = i + 1
        end do
        
        write(*,*) "useless iteration number for getting R-Maxwellian =", cyc
        
        do k=1,n0
           
           xh = 1.0 + (real(k)-0.5)/real(n0)*real(m-1) 
           
           gamt = sqrt(1.0 + dot_product(u(k,1:3,s),u(k,1:3,s)))
           v(k,1:3,s) = u(k,1:3,s)/gamt
           x(k,1,s) = xh + 0.5*v(k,1,s)
           
           if(x(k,1,s)>=real(m)) then
              x(k,1,s) = x(k,1,s) - real(m) + 1.0
           else if(x(k,1,s)<1.0) then
              x(k,1,s) = x(k,1,s) + real(m) - 1.0
           end if
           
        end do
        
        deallocate(r1)
        deallocate(r2)
        deallocate(r3)     
        
     end if
  end do

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!! set "e" and "p" same position !!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  x(1:n0,1,2) = x(1:n0,1,1)

!!!!!!!!! initial "density" !!!!!!
  pden(1:m) = 0.0
  nden(1:m) = 0.0
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!        initial electro-magnetic field         !!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  e(1:m,1) = 0.0
  e(1:m,2) = 0.0
  e(1:m,3) = 0.0
  b(1:m,1) = 0.0
  b(1:m,2) = 0.0
  b(1:m,3) = 0.0
  pondf(1:n0,1:2) = 0.0
  !casenum==0:sin^2 envelope with user set wavenum, length, and amplitude
  !casenum==1:gaussian with user set length and amplitude
  !casenum==2:lop-sided gaussian with user set length, amplitude
  !casenum==3:lop-sided gaussian with user set length, amplitude
  !pulselength=0.5
  gausstemp1=4.0/((pulselength*m/ss)**2)
  gausstemp2=100.0/((pulselength*m/ss)**2)
  gausstemp3=1.2345679/((pulselength*m/ss)**2)

  if(casenum == 0)then
      ioffset=int(pulselength*2*m/ss)
      do k=1,ioffset
         tempf=pi*(k-1)/ioffset
         e(k,2)=e0(2)*sin(wavenum*2*tempf)*sin(tempf)*sin(tempf)
      enddo
  elseif(casenum==1)then
      ioffset=int(1.6*pulselength*m/ss)
      do k=1,m
         tempf=(k-1-ioffset)**2
         e(k,2)=e0(2)*exp(-real(tempf*gausstemp1))
      enddo
  elseif(casenum==2)then
      ioffset=int(1.2*pulselength*m/ss)
      do k=1,ioffset-1
         tempf=(k-1-ioffset)**2
         e(k,2)=e0(2)*exp(-real(tempf*gausstemp2))
      enddo
      do k=ioffset,m
         tempf=(k-1-ioffset)**2
         e(k,2)=e0(2)*exp(-real(tempf*gausstemp3))
      enddo
  elseif(casenum==3)then
      ioffset=int(2.0*pulselength*m/ss)
      do k=1,ioffset-1
         tempf=(k-1-ioffset)**2
         e(k,2)=e0(2)*exp(-real(tempf*gausstemp3))
      enddo
      do k=ioffset,m
         tempf=(k-1-ioffset)**2
         e(k,2)=e0(2)*exp(-real(tempf*gausstemp2))
      enddo
  elseif(casenum==4)then
      do k=1,m
         tempf=pi*(k-1)/m
         e(k,2)=e0(2)*sin(wavenum*2*tempf)
      enddo
  endif
  b(1:m,3)=e(1:m,2) !set Bz=Ey for linearly polarized plane wave

  return
end subroutine initial

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
subroutine reinitial(e,b,x,u,v,pden,nden,pondf)

  use param , only :n0, m, B0, E0, ppg, sigma, pi, wavenum, runnum
  use param , only :ss, ts, dx, time, pi, cc, econ ,q, dt, pulselength
  use param , only :ub, vb, gamb, uth, vth, gamth, dl, casenum
  use omp_lib  

  implicit none
  
  real(8), intent(out) :: pden(:), nden(:)  
  real(8), intent(out) :: e(:,:), b(:,:), pondf(:,:)
  real(8), intent(out) :: x(:,:,:), u(:,:,:), v(:,:,:)
  real(8) :: xpos, gamt(1:2), xtemp1(1:n0), xtemp2(1:n0)

  integer :: k, i
  integer :: ioffset
  integer :: nthreads

  open(13, file='field.dat')
  open(20, file='positive.dat')
  open(21, file='negative.dat')

  do i=1,m-1
!!! grid information !!!
     read(13,'(8e15.7)')e(i,1),e(i,2),e(i,3),b(i,1),b(i,2),b(i,3),pden(i),nden(i)
  end do

  do k=1,n0
!!! phase space > phase.dat !!!
     read(20,'(9e15.7)') x(k,1,1), x(k,2,1), x(k,3,1), v(k,1,1), v(k,2,1), v(k,3,1), u(k,1,1), u(k,2,1), u(k,3,1)
     read(21,'(9e15.7)') x(k,1,2), x(k,2,2), x(k,3,2), v(k,1,2), v(k,2,2), v(k,3,2), u(k,1,2), u(k,2,2), u(k,3,2)
  end do

  pondf(1:n0,1:2)=0.0

  close(13)
  close(20)
  close(21)


  return
end subroutine reinitial
