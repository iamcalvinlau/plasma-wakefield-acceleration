program main

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!                                                             !!!
!!! special relativistic one-dimensional Partielce-in-Cell code !!!
!!!                                                             !!!
!!! boundary condition   periodic                               !!!
!!!                                                             !!!
!!! written by Kentaro NAGATA,  Osaka University,   2005/12/06  !!!
!!!                                                             !!!
!!!  update    2006/05/21                                       !!!
!!!                minor change                                 !!! 
!!!            2006/02/25                                       !!! 
!!!                grid vectors (gp,g0,gm) inserted             !!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  
  use param , only :n0, m, time, runnum
  use param , only :initial_cal, ppg, monse, monsh, dispst
  use omp_lib

  implicit none
  
  real :: start_time, end_time, total_time
  integer :: nthreads
  real(8), allocatable :: pden(:), nden(:)
  real(8), allocatable :: e(:,:), b(:,:), cur(:,:), pondf(:,:)
  real(8), allocatable :: x(:,:,:), u(:,:,:), v(:,:,:)  
  integer :: t

  interface
     
     subroutine initial(e,b,x,u,v,pden,nden,pondf)
       implicit none
       real(8), intent(out) :: pden(:), nden(:)
       real(8), intent(out) :: e(:,:), b(:,:), pondf(:,:)
       real(8), intent(out) :: x(:,:,:), u(:,:,:), v(:,:,:)
     end subroutine initial

     subroutine reinitial(e,b,x,u,v,pden,nden,pondf)
       implicit none
       real(8), intent(out) :: pden(:), nden(:)
       real(8), intent(out) :: e(:,:), b(:,:), pondf(:,:)
       real(8), intent(out) :: x(:,:,:), u(:,:,:), v(:,:,:)
     end subroutine reinitial
     
     subroutine density(x,v,e,cur,pden,nden) 
       implicit none              
       real(8), intent(in) :: x(:,:,:), v(:,:,:)
       real(8), intent(out) :: e(:,:), cur(:,:)
       real(8), intent(out) :: pden(:), nden(:)
     end subroutine density

     subroutine motion(e,b,x,u,v,pondf)
       implicit none
       real(8), intent(in) :: e(:,:), b(:,:)     
       real(8), intent(inout) :: x(:,:,:), u(:,:,:)
       real(8), intent(out) ::  v(:,:,:), pondf(:,:)
     end subroutine motion

     subroutine fsolve(e,b,cur)
       implicit none  
       real(8), intent(inout) :: e(:,:), b(:,:)
       real(8), intent(in) :: cur(:,:)
     end subroutine fsolve

     subroutine energy(e,b,t,u,x)
       implicit none       
       integer, intent(in) :: t
       real(8), intent(in) :: e(:,:), b(:,:),u(:,:,:),x(:,:,:)
     end subroutine energy

     subroutine history(x,u,e,b,cur,t,pden,nden,pondf)
       implicit none
       integer, intent(in) :: t
       real(8), intent(in) :: pden(:), nden(:)
       real(8), intent(in) :: e(:,:), b(:,:), cur(:,:)
       real(8), intent(in) :: x(:,:,:), u(:,:,:), pondf(:,:)
     end subroutine history
  
     subroutine print(e,b,x,u,v,pden,nden)
       implicit none
       real(8), intent(in) :: pden(:),nden(:)
       real(8), intent(in) :: e(:,:), b(:,:)
       real(8), intent(in) :: u(:,:,:) ,x(:,:,:),v(:,:,:)
     end subroutine print
     
  end interface
  
  call initial_cal  
 
  if(runnum==0)then 
      open(18, file='energy.dat')
      open(22, file='momentx.dat')
  else
      open(18, file='energy.dat',access='append')
      open(22, file='momentx.dat',access='append')
  endif
 
!  open(23, file='momenty.dat')  
!  open(24, file='momentz.dat')  


  allocate(x(1:n0,1:1,1:2))
  allocate(u(1:n0,1:3,1:2))
  allocate(v(1:n0,1:3,1:2))
  allocate(e(1:m,1:3))
  allocate(b(1:m,1:3))
  allocate(cur(1:m,1:3))
  allocate(pden(1:m))
  allocate(nden(1:m))
  allocate(pondf(1:n0,1:2))  

  if(runnum==0)then
      call initial(e,b,x,u,v,pden,nden,pondf)
  else
      call reinitial(e,b,x,u,v,pden,nden,pondf)
  endif  

!  time=-1
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!! time iteration start !!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  write(*,*) "start iteration"
  !open(1021,file="cc_check.dat")
  open(1022,file="field_check.dat")
  call cpu_time(start_time)

  do t=0,time

     !if(mod(t,dispst)==0) write(*,*) t,"/",time        
     
     call density(x,v,e,cur,pden,nden)
     
     call fsolve(e,b,cur)
     
     call motion(e,b,x,u,v,pondf)
     
     if(mod(t,monse)==0) then
        call energy(e,b,t,u,x)
     end if
     
     if(mod(t,monsh)==0) then
        call history(x,u,e,b,cur,t,pden,nden,pondf)
     end if
     
  end do
  call cpu_time(end_time)

!!!!!!!!!!!!!!!!!!!!!!!!!!
!!! time iteration end !!!
!!!!!!!!!!!!!!!!!!!!!!!!!!
  close(18)
  close(22)
  !close(1021)
  close(1022)
!  close(23)  
!  close(24)    

  write(*,*) "end iteration"

! information on how much parallel data saves..
  total_time=end_time-start_time
  open(12, file='RUN_TIME')
  write(12,*) 'start time =', start_time, "s"
  write(12,*) 'end time =', end_time, "s"
  write(12,*) 'total cpu time=', total_time, "s"

!$omp parallel private(nthreads)
  nthreads = omp_get_num_threads()
!$omp single
  write(12,*) 'number of omp threads =', nthreads
  write(12,*) 'total user time=', real(total_time/nthreads), "s"
!$omp end single nowait
!$omp end parallel

  close(12)
  call print(e,b,x,u,v,pden,nden)
  

end program main
