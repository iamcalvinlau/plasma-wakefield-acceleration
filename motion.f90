subroutine motion(e,b,x,u,v,pondf)

  use param , only :m, dt, q, gam0, dx, mass, n0, gp

  implicit none
  
  real(8), intent(in) :: e(:,:), b(:,:)
  real(8), intent(inout) :: x(:,:,:), u(:,:,:)
  real(8), intent(out) ::  v(:,:,:), pondf(:,:)

  real(8) :: gamt
  real(8) :: ep(1:3), bp(1:3)
  integer :: s, k, i
  real(8) :: um(1:3), up(1:3), Bt(1:3), vpro(1:3), vproa(1:3)
  real(8) :: const0, dp1, dp2
  real(8) :: distp, distm

  do s=1,2
     
     const0 = 0.5*gam0*q(s)*dt/mass(s)     
!$omp parallel do private(k,i,distp,distm,ep,bp,um,&
!$omp& dp1,bt,vpro,vproa,dp2,up,gamt) 
     do k=1,n0
!!! electro-magnetic field on each particles !!!
        i =  int(x(k,1,s))
        distp = x(k,1,s) - real(i)
        distm = 1.0 - distp

        ep(1:3) = distm*e(i,1:3) + distp*e(gp(i),1:3)
        bp(1:3) = distm*b(i,1:3) + distp*b(gp(i),1:3)
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        
        
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!         solve equation of relativiatic motion      !!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        
!!! first acceleration by electric field !!!
        um(1:3) = u(k,1:3,s) + const0*ep(1:3)
!!! preparing for velocity rotation !!!
        dp1 = sqrt(1.0 + dot_product(um(1:3),um(1:3)))
        bt(1:3) = const0*bp(1:3)/dp1        
!!! vector product !!!
        vpro(1) = um(2)*bt(3) - um(3)*bt(2)
        vpro(2) = um(3)*bt(1) - um(1)*bt(3)
        vpro(3) = um(1)*bt(2) - um(2)*bt(1)
        
        vproa(1) = vpro(2)*bt(3) - vpro(3)*bt(2)
        vproa(2) = vpro(3)*bt(1) - vpro(1)*bt(3)
        vproa(3) = vpro(1)*bt(2) - vpro(2)*bt(1)
!!! velocity rotation !!!
        dp2 = 2.0/(1.0+dot_product(bt(1:3),bt(1:3)))
        up(1:3) = um(1:3) + (vpro(1:3) + vproa(1:3))*dp2
!!! second acceleration by electric field !!!
        u(k,1:3,s) = up(1:3) + const0*ep(1:3)
!!! new values !!!
        gamt = sqrt(1.0 + dot_product(u(k,1:3,s),u(k,1:3,s)))        
        v(k,1:3,s) = u(k,1:3,s)/gamt
        
        pondf(k,s) = q(s)*v(k,2,s)*bp(3)      

        x(k,1,s)  = x(k,1,s) + v(k,1,s)
!!!!!!!!!!!!! periodic boundary condition !!!!!!!!!!!!!
        if(x(k,1,s)>=real(m)) then
           x(k,1,s) = x(k,1,s) - real(m) + 1.0
        else if(x(k,1,s)<1.0) then
           x(k,1,s) = x(k,1,s) + real(m) - 1.0
        end if
     end do
!$omp end parallel do
  end do
  
end subroutine motion
