#!bin/sh

set lmargin 5
set rmargin 1
set tmargin 0
set xrange [0:1]
set term png

set size 1,1
set out"energy&momentum.png"
set multiplot

set size 1,0.5
set origin 0,0.5
set format x""
set bmargin 0
set tmargin 1
#set yrange [0:1]
plot "energy.dat" u 1:2 w lt 3 lc 1,\
"energy.dat" u 1:3 w l lt 2 lc 3,\
"energy.dat" u 1:4 w l lt 1 lc 7

set size 1,0.5
set origin 0,0
set format x""
set bmargin 1
set tmargin 0
#set yrange [0:1]
plot "momentx.dat" u 1:2 w lt 3 lc 1,\
"momentx.dat" u 1:3 w l lt 2 lc 3,\
"momentx.dat" u 1:4 w l lt 1 lc 7

unset multiplot

#set nomultiplot
